# Field Encryption

Zero-dep encryption library for Mongoose schemas.

## Install

```bash
yarn add @raggesilver/field-encryption
# or npm i @raggesilver/field-encryption # if you're into that
```

## Use

```javascript
const mongoose = require('mongoose');
const fieldEncryption = require('@raggesilver/field-encryption');

const schema = new mongoose.Schema({
  name: String,
  documents: {
    ssn: String,
  },
  address: String,
});

schema.plugin(fieldEncryption, {
  // Secret used to encrypt/decrypt data
  secret: 'MY_SUPER_SECRET',
  // These fields will be encrypted
  fields: ['documents.ssn', 'address'],
});

const model = mongoose.model('user', schema);
```

Encryption and decryption are automatic, when you `.save()` a document the
right fields will be encrypted and sent to the database (same goes for update
queries). Upon fetch all encrypted fields will be decrypted.

Field Encryption will also add two **methods** to your documents: `.encrypt()`
and `.decrypt()`. You don't need to use these functions for anything, unless
you want the local document to be encrypted (`.encrypt()`), then to undo the
local encryption (`.decrypt()`).

# Credits

This library is mostly derivative work from
[mongoose-field-encryption](https://github.com/wheresvic/mongoose-field-encryption).
It's reason to exist is that `mongoose-field-encryption` doesn't handle nested
fields.
