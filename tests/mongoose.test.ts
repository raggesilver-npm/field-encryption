import { Schema, Document, model } from 'mongoose';

const schema = new Schema({
  name: String,
  age: Number,
  documents: {
    ssn: String,
  },
});

export interface ISchema extends Document {
  name: string;
  age: number;
  documents: {
    ssn: string;
  };
}

export const ShemaModel = model<ISchema>('model', schema);

describe('Ensure Mongoose behavior is what we expect', () => {
  it('Schema types', () => {
    expect(schema.path('name')).toBeInstanceOf(Schema.Types.String);
    expect(schema.path('age')).toBeInstanceOf(Schema.Types.Number);
    expect(schema.path('documents')).toBeUndefined();
    expect(schema.path('documents.ssn')).toBeInstanceOf(Schema.Types.String);
  });
});
