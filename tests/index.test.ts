import mongoose, { Document, Schema } from 'mongoose';
import fieldEncryption, { EncryptedDocument } from '../src/field-encryption';
import _ from '@raggesilver/hidash';

const DB_URL = process.env.DB_URL || 'mongodb://localhost/field_encryption';

const userSchema = new mongoose.Schema({
  name: String,
  documents: {
    ssn: String,
    driversLicense: {
      number: String,
      exp: String,
      dob: String,
    },
  },
});

userSchema.plugin(fieldEncryption, {
  secret: 'CI_SUPER_SECRET',
  fields: [
    'documents.ssn',
    'documents.driversLicense.number',
  ],
});

interface IUser extends Document {
  name: string;
  documents: {
    ssn: string;
    driversLicense: {
      number: string;
      exp: string;
      dob: string;
    }
  }
}

const userModel = mongoose.model<IUser>('user', userSchema);

beforeAll(async function () {
  await mongoose.connect(DB_URL)
    .then(() => userModel.deleteMany({}));
});

it('Auto-encryption on document.save()', async () => {
  expect.assertions(5);

  const u = new userModel({
    name: 'John Doe',
    documents: {
      ssn: '000000000',
      driversLicense: {
        number: '123',
        exp: '1610129999406',
        dob: '1995-01-01T03:00:00.000Z',
      },
    },
  });

  await u.save()
    // The local document should be untouched
    .then(() => expect(u.documents.ssn).toBe('000000000'))
    .then(() => userModel.findOne({
      // We shouldn't be able to find it because documents.ssn is encrypted
      _id: u._id, 'documents.ssn': u.documents.ssn,
    }))
    .then(doc => expect(doc).toBeNull());

  // Make sure no raw info is being stored in the DB
  await mongoose.connection.db.collection('users').findOne({ _id: u._id })
    .then(doc => {
      expect(doc).not.toBeNull();
      expect(doc!.documents.ssn).not.toBe(u.documents.ssn);
      expect(doc!.documents.ssn).toMatch(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/);
    });
});

it('Auto-encryption on document.updateOne()', async () => {
  expect.assertions(4);

  const u = new userModel({
    name: 'Doe John',
    documents: {
      ssn: '000000000',
    },
  });

  await u.save();
  await u.updateOne({ $set: { 'documents.ssn': '000000123' }});

  await userModel.findById(u._id)
    .then(doc => {
      expect(doc).not.toBeNull();
      expect(doc!.documents.ssn).toBe('000000123');
    });

  // Make sure no raw info is being stored in the DB
  await mongoose.connection.db.collection('users').findOne({ _id: u._id })
    .then(doc => {
      expect(doc).not.toBeNull();
      expect(doc!.documents.ssn).toMatch(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/);
    });
});

it('Auto-encryption on model.updateOne()', async () => {
  expect.assertions(4);

  const u = new userModel({
    name: 'Doe John',
    documents: {
      ssn: '000000333',
    },
  });

  await u.save();
  await userModel
    .updateOne({ _id: u._id }, { $set: { 'documents.ssn': '000000444' }});

  await userModel.findById(u._id)
    .then(doc => {
      expect(doc).not.toBeNull();
      expect(doc!.documents.ssn).toBe('000000444');
    });

  // Make sure no raw info is being stored in the DB
  await mongoose.connection.db.collection('users').findOne({ _id: u._id })
    .then(doc => {
      expect(doc).not.toBeNull();
      expect(doc!.documents.ssn).toMatch(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/);
    });
});

it('Auto-encryption on model.updateMany()', async () => {
  expect.assertions(3);

  await userModel
    .updateMany({}, { $set: { 'documents.ssn': '000000123' }})
    .then(up => {
      expect(up.acknowledged).toBeTruthy();
      expect(up.modifiedCount).toBe(3);
    });

  // Make sure no raw info is being stored in the DB
  await mongoose.connection.db.collection('users')
    .findOne({ 'documents.ssn': '000000123' })
    .then(doc => expect(doc).toBeNull());
});

it('Auto-encryption on model.findOneAndUpdate()', async () => {
  expect.assertions(1);
  const u = new userModel({
    name: 'Doe John',
    documents: {
      ssn: '000000334',
    },
  });

  await u.save();
  await userModel.findOneAndUpdate(
    { _id: u._id }, { $set: { 'documents.ssn': '000009929' }}
  );

  // Make sure we can't find the updated doc by it's raw SSN
  const nu = await userModel.findOne({ 'documents.ssn': '000009929' });
  expect(nu).toBeNull();
});

it('Ensure there is no unencrypted data in the DB', async () => {
  expect.assertions(1);
  await userModel.find({ $or: [
    { 'documents.ssn': { $regex: /^\d+$/ }},
    { 'documents.driversLicense.number': { $regex: /^\d+$/ }},
  ]})
    .then(docs => expect(docs).toHaveLength(0));
});

// Objects & Arrays ============================================================

const personSchema = new mongoose.Schema({
  phone: {
    cc: String,
    number: String,
  },
  documents: {
    ssn: String,
    public: String,
  },
  age: Number,
  friends: { type: [String] },
  name: String,
  related: mongoose.Schema.Types.ObjectId,
});

const personEncryptedFields = ['phone', 'age', 'friends', 'name', 'related', 'documents.ssn'];
const personNonStringEncryptedFields = personEncryptedFields.filter(f => f !== 'name');

personSchema.plugin(fieldEncryption, {
  secret: 'AAA', fields: personEncryptedFields,
});

interface IPerson extends EncryptedDocument {
  phone: {
    cc: string;
    number: string;
  };
  documents: {
    ssn: string;
    public: string;
  };
  age: number;
  friends: string[];
  name: string;
  related: mongoose.Schema.Types.ObjectId;
}

const Person = mongoose.model<IPerson>('person', personSchema);

describe('Test encryption on nested objects & arrays', () => {
  it('Auto-encryption on objects & arrays', async () => {
    const p = new Person({
      phone: {
        cc: '1',
        number: '(310) 337-7777',
      },
      documents: {
        ssn: '123456789',
        public: '123',
      },
      age: +100,
      friends: ['Lillia', 'Fiddlesticks', 'Renekton'],
      name: 'Rito Gomes',
      related: new mongoose.Types.ObjectId(),
    });

    await p.save();

    const original = JSON.stringify(p);
    const originalObj = <IPerson> p.toObject();

    p.encrypt();

    expect(JSON.stringify(p)).not.toEqual(original);

    const encObj = <any> p.toObject();
    personEncryptedFields.forEach(_key => {
      // Original key
      const key = _key as keyof IPerson;
      const encKey = `__rsfe_${key}`;

      if (!(personSchema.path(key) instanceof Schema.Types.String)) {
        // Encrypted key
        expect(_.get(encObj, key)).toBeUndefined();
        expect(_.get(encObj, encKey)).toMatch(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/);
        expect(_.get(encObj, encKey)).not.toBe(originalObj[key]);
      }
      else {
        expect(_.get(encObj, key)).toMatch(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/);
      }
    });

    const finds = await Promise.all(Object.keys(originalObj)
      .filter(k => !['_id', '__v'].includes(k))
      .map(_key => {
        const key = _key as keyof IPerson;
        return Person.findOne({ _id: p._id, [key]: originalObj[key] });
      }));

    expect(finds.every(e => e === null)).toBe(true);

    p.decrypt();

    expect(JSON.stringify(p)).toBe(original);
    expect(p.toObject()).toEqual(originalObj);
  });

  it('Make sure fields with name prefixes are properly decrypted & renamed', async () => {
    const p = new Person({
      phone: {
        cc: '1',
        number: '(310) 337-7777',
      },
      documents: {
        ssn: '123456789',
        public: '123',
      },
      age: +100,
      friends: ['Lillia', 'Fiddlesticks', 'Renekton'],
      name: 'Rito Gomes',
      related: new mongoose.Types.ObjectId(),
    });

    await p.save();

    const docs = (await Promise.all([
      Person.findById(p._id),
      Person.findOne({ _id: p._id }),
      Person.find({ _id: p._id }),
    ]))
      .map(e => e instanceof Array ? (e[0] || null) : e);

    docs.forEach(d => {
      expect(d).not.toBeNull();
      expect(d?._id.toString()).toBe(p._id.toString());
      personNonStringEncryptedFields.forEach(f => {
        expect(JSON.stringify(d?.get(f)))
          .toBe(JSON.stringify(p.get(f)));
      });
    });
  });

  /**
   * Make sure a field that is supposed to be encrypted but it's actually raw
   * in the database won't break the `decrypt` function
   */
  it('Ensure raw data from the DB doesn\'t break the decrypt function', async () => {
    // Doc lacking `name` field
    const p = new Person({
      phone: {
        cc: '1',
        number: '(310) 337-7777',
      },
      documents: {
        ssn: '123456789',
        public: '123',
      },
      age: +100,
      friends: ['Lillia', 'Fiddlesticks', 'Renekton'],
      related: new mongoose.Types.ObjectId(),
    });

    await p.save();

    // Manually set a raw value for the missing `name` field
    await mongoose.connection.collection('people')
      .findOneAndUpdate({_id:p._id}, { $set: { name: 'Rito Gomes' }});

    // Retrieve the document containing raw value for a field supposed to be
    // encrypred
    const up = await Person.findById(p._id);

    expect(up?.name).toBe('Rito Gomes');
  });
});

describe('Test throws', () => {
  const s = new mongoose.Schema({ name: String });

  it('Should throw due to missing secret', () => {
    expect(() => s.plugin(fieldEncryption, {})).toThrowError('Invalid/missing secret');
  });

  it('Should throw due to invalid field', () => {
    expect(() => s.plugin(fieldEncryption, {
      fields: ['batata'],
      secret: 'AAAA',
    })).toThrowError('\'batata\' is not part of schema');
  });
});

afterAll(done => {
  userModel.deleteMany()
    // .then(() => Person.deleteMany())
    .then(() => mongoose.disconnect())
    .then(done);
});
