import * as crypto from 'crypto';
import { Schema, SchemaType, Document, Error, Query, UpdateQuery } from 'mongoose';

type HookNext = (err?: Error | null) => void;

type PatchedSchemaType = SchemaType & {
  instance: string;
};

export interface EncryptedDocument extends Document {
  encrypt: (this: Document) => void;
  decrypt: (this: Document) => void;
}

function encrypt (value: any, secret: string) {
  const opts: any = {};

  if (typeof value !== 'string') {
    value = JSON.stringify(value);
    opts.parse = true;
  }

  const salt = crypto.randomBytes(16);
  const cip = crypto.createCipheriv('aes-256-cbc', secret, salt);
  const enc = cip.update(value);
  const final = Buffer.concat([ enc, cip.final() ]);

  return (
    `${salt.toString('hex')}:${final.toString('hex')}:${JSON.stringify(opts)}`
  );
}

function decrypt <T> (value: string | T, secret: string): T {
  if (typeof value !== 'string') {
    return value as T;
  }

  const [full, rawSalt, rawEnc, rawOpts] =
    value.match?.(/([a-f0-9]+):([a-f0-9]+):(\{.*\})/) || [];

  if (!full) {
    return value as T;
  }

  const salt = Buffer.from(rawSalt, 'hex');
  const enc = Buffer.from(rawEnc, 'hex');
  const decip = crypto.createDecipheriv('aes-256-cbc', secret, salt);
  const dec = decip.update(enc);
  let raw: string | T = Buffer.concat([ dec, decip.final() ]).toString();

  if (JSON.parse(rawOpts).parse === true) {
    raw = JSON.parse(raw);
  }

  return raw as T;
}

export interface IEncryptionOptions {
  secret: string;
  fields: string[];
}

export default function (schema: Schema, options: IEncryptionOptions) {
  if (typeof options?.secret !== 'string') {
    throw new Error('Invalid/missing secret');
  }

  const fields = options.fields || [];
  const secret = crypto
    .createHash('sha256')
    .update(options.secret)
    .digest('hex')
    .substr(0, 32);

  for (const f of fields) {
    if (schema.pathType(f) === 'adhocOrUndefined') {
      throw new Error(`'${f}' is not part of schema`);
    }
  }

  function encryptFields (doc: Document, force = false) {
    for (const f of fields) {
      const val = doc.get(f);

      // If the field does not exist or it isn't modified
      if (val === undefined || (!doc.isModified(f) && !doc.isNew && !force)) {
        continue;
      }

      if ((schema.path(f) as PatchedSchemaType)?.instance === 'String') {
        doc.set(f, encrypt(val, secret));
      }
      else {
        doc.set(f, undefined);
        doc.set(`__rsfe_${f}`, encrypt(val, secret), { strict: false });
      }
    }
  }

  function decryptFields (doc: Document) {
    for (const f of fields) {
      const alternate = ((doc.schema.path(f) as PatchedSchemaType)?.instance === 'String')
        ? undefined : `__rsfe_${f}`;

      const val = doc.get(alternate || f, );

      if (val === undefined) {
        continue;
      }

      doc.set(f, decrypt(val, secret)).unmarkModified(f);

      if (alternate) {
        doc.set(alternate, undefined, { strict: false })
          .unmarkModified(alternate);
      }
    }
  }

  schema.pre('save', function (next) {
    try {
      encryptFields(this);
      next();
    }
    catch (e) {
      next(e as any);
    }
  });

  schema.post('save', function (_doc, next) {
    try {
      decryptFields(this);
      next();
    }
    catch (e) {
      next(e as any);
    }
  });

  schema.post('init', function (doc) {
    decryptFields(doc);
  });

  function preUpdateHook (this: Query<any, any>, next: HookNext) {
    const qr = this.getUpdate() as UpdateQuery<any>;

    // console.log('Pre update hook', qr?.$set);

    if (!qr?.$set) {
      return next();
    }

    // console.log('Pre update hook -> fields', fields);

    const $set = { ...qr.$set };

    for (const f in $set) {
      // console.log('Pre update hook ->', f, 'in', fields, fields.includes(f));
      if (fields.includes(f)) {
        const v = encrypt($set[f], secret);
        // console.log('Pre update hook -> enc', f, 'from', $set[f], 'to', v);
        $set[f] = v;
        // console.log('Pre update hook -> enced', $set[f]);
      }
    }

    // console.log('Pre update hook -> after enc', $set);
    this.setUpdate({ ...qr, $set });
    // console.log('Pre update hook -> after set', this.getUpdate()?.$set);
    next();
  }

  (schema.pre as any)('findOneAndUpdate', preUpdateHook);

  (schema.pre as any)(
    /update.*/, { query: true, document: false }, preUpdateHook
  );

  /**
   * WARNING: encrypting the fields locally will cause update/save errors, use
   * this function only for logging
   */
  schema.methods.encrypt = function (this: Document) {
    encryptFields(this, true);
  };

  schema.methods.decrypt = function (this: Document) {
    decryptFields(this);
  };
}
